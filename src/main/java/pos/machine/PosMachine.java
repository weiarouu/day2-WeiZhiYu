package pos.machine;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        LinkedHashMap<String, Integer> counter = barCodeCounter(barcodes);
        return generateReceipt(counter);
    }

    public LinkedHashMap<String, Integer> barCodeCounter(List<String> barcodes) {
        LinkedHashMap<String, Integer> counter = new LinkedHashMap<>();
        barcodes.forEach((barcode) -> {
            if (counter.containsKey(barcode)) {
                int oldCount = counter.get(barcode);
                counter.put(barcode, oldCount + 1);
            } else {
                counter.put(barcode, 1);
            }
        });
        return counter;
    }

    public LinkedHashMap<Item, Integer> convertBarcodeToItem(LinkedHashMap<String, Integer> barcodeCounts, List<Item> items) {
        LinkedHashMap<Item, Integer> itemCounts = new LinkedHashMap<>();
        barcodeCounts.forEach((barcode, count) -> {
            Item correspondItem = items.stream().filter(item -> barcode.equals(item.getBarcode()))
                    .findAny()
                    .orElse(null);
            itemCounts.put(correspondItem, count);
        });
        return itemCounts;
    }

    public String generateTotalLine(LinkedHashMap<Item, Integer> itemCounts) {
        int total = 0;
        for (Map.Entry<Item, Integer> itemCount: itemCounts.entrySet()) {
            total = total + itemCount.getKey().getPrice() * itemCount.getValue();
        }
        return String.format("Total: %d (yuan)", total);
    }

    public String generateSingleCell(String cellName, String cellValue) {
        return String.format("%s: %s", cellName, cellValue);
    }

    public String generateItemLine(Item item, Integer count) {
        String nameCell = generateSingleCell("Name", item.getName());
        String quantityCell = generateSingleCell("Quantity", count.toString());
        String unitPriceCell = generateSingleCell("Unit price", item.getPrice() + " (yuan)");
        String subTotalCell = generateSingleCell("Subtotal", item.getPrice()*count + " (yuan)");
        return String.format("%s, %s, %s, %s", nameCell, quantityCell, unitPriceCell, subTotalCell);
    }

    public String generateReceipt(LinkedHashMap<String, Integer> barcodeCounts) {
        ArrayList<String> receiptLines = new ArrayList<String>();
        String receiptTitle = "***<store earning no money>Receipt***";
        receiptLines.add(receiptTitle);

        List<Item> items = ItemsLoader.loadAllItems();
        LinkedHashMap<Item, Integer> itemCounts = convertBarcodeToItem(barcodeCounts, items);

        String totalLine = generateTotalLine(itemCounts);

        barcodeCounts.forEach((barcode, count) -> {
            Item correspondItem = items.stream().filter(item -> barcode.equals(item.getBarcode()))
                    .findAny()
                    .orElse(null);
            String currentItemLine = generateItemLine(correspondItem, count);
            receiptLines.add(currentItemLine);
        });

        receiptLines.add("----------------------");
        receiptLines.add(totalLine);
        receiptLines.add("**********************");

        return String.join("\n", receiptLines);

    }

}
